package com.live.elisvobs.bincard;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.spinner) MaterialBetterSpinner mBetterSpinner;
    TextInputEditText supplier, account, name, unit, stock, remarks;
    private static final String[] measurementUnits = {" ", "g", "kg", "l", "ml", "t"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        supplier = findViewById(R.id.supplierName);
        account = findViewById(R.id.accountNumber);
        name = findViewById(R.id.itemLabel);
        unit = findViewById(R.id.unitMeasurement);
        stock = findViewById(R.id.stockRefNo);
        remarks = findViewById(R.id.itemRemarks);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line, measurementUnits);
        mBetterSpinner.setAdapter(adapter);

        mBetterSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getBaseContext(), parent.getItemAtPosition(position) + " selected", Toast.LENGTH_LONG).show();
                //Save and display selected item on the list
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @OnClick(R.id.nextButton)
    public void startBinActivity(View view) {
        Intent intent = new Intent(this,BinActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.viewButton)
    public void startItemsActivity(View view) {
        Intent intent = new Intent(this, ItemsActivity.class);
        startActivity(intent);
    }
}
