package com.live.elisvobs.bincard;

public class Item {
    private String itemName, symbolName, stockBalance;

    public Item() {
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getSymbolName() {
        return symbolName;
    }

    public void setSymbolName(String symbolName) {
        this.symbolName = symbolName;
    }

    public String getStockBalance() {
        return stockBalance;
    }

    public void setStockBalance(String stockBalance) {
        this.stockBalance = stockBalance;
    }
}
