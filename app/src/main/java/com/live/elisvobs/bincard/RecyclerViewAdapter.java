package com.live.elisvobs.bincard;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private Context context;
    private List<Item> ItemList;

    public RecyclerViewAdapter(Context context, List<Item> itemList) {
        this.context = context;
        ItemList = itemList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.model, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Item item = ItemList.get(position);
        holder.itemLabel.setText(item.getItemName());
        holder.symbolName.setText(item.getSymbolName());
        holder.stockBalance.setText(item.getStockBalance());
    }

    @Override
    public int getItemCount() {
        return ItemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final TextView itemLabel,symbolName, stockBalance;

        public ViewHolder(View itemView) {
            super(itemView);
            itemLabel = itemView.findViewById(R.id.itemName);
            symbolName = itemView.findViewById(R.id.symbolName);
            stockBalance = itemView.findViewById(R.id.stockBalance);
        }
    }
}
